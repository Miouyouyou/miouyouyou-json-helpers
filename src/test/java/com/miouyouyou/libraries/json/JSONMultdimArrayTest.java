package com.miouyouyou.libraries.json;

import org.json.JSONArray;

import static com.miouyouyou.libraries.json.JSONNestedArraysToArray.to_array;
import static com.miouyouyou.libraries.json.JSONNestedArraysToArray.min_dimensions;
import com.miouyouyou.libraries.json.interfaces.JSONArraysExtractor;
import com.miouyouyou.libraries.json.extractors.JSONNumberExtractor;

import junit.framework.Assert;
import junit.framework.TestCase;

public class JSONMultdimArrayTest extends TestCase {
  public String json_int_array_string, only_one_dimension, 
    min_dimension_checkstring;
  public JSONArray json_int_array;
  
  
  public void setUp() throws Exception {
    super.setUp();
    this.json_int_array_string = "[[1, 2], [3, 4, 5], [6], [[09,77]] ]";
    this.json_int_array = new JSONArray(json_int_array_string);
    this.only_one_dimension =
      "[{value: [1, 2, 3]}, \"[4, 5, 6]\", '[10, 11, 12]', 7, 8 ,9]";
    this.min_dimension_checkstring =
      "[ { \"a\": \"'\" }, \"{\", [1, 2, 3]]";
  }

  public void testMultidim() throws Exception {
    checkIntArray((int[][]) to_array(this.json_int_array, int.class));
    checkIntArray((int[][]) to_array(this.json_int_array_string, int.class));
                                                           
    JSONArraysExtractor int_extractor =
      new JSONNumberExtractor(int.class);
    checkIntArray((int[][]) to_array(this.json_int_array, int_extractor));
    checkIntArray((int[][]) to_array(this.json_int_array_string,
                                     int_extractor));
  }

  public void testMinDimensions() throws Exception {
    Assert.assertEquals(2, min_dimensions(this.min_dimension_checkstring));
  }
  
  public void checkIntArray(int[][] array) {
    Assert.assertEquals(4, array.length);
    Assert.assertEquals(2, array[0].length);
    Assert.assertEquals(3, array[1].length);
    Assert.assertEquals(1, array[2].length);
    Assert.assertEquals(4, array[1][1]);
    Assert.assertEquals(0, array[3].length);  
  }
  
  public void testFalseDimensions() {
    byte[] array = (byte[]) to_array(this.only_one_dimension,
                                     new JSONNumberExtractor(byte.class));
    
    Assert.assertEquals(3, array.length);
    Assert.assertEquals(7, array[0]);
    Assert.assertEquals(8, array[1]);
    Assert.assertEquals(9, array[2]);
  }
}
