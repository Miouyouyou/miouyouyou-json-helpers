package tests.com.miouyouyou.libraries.json;

import com.miouyouyou.libraries.json.LocalParser;

import junit.framework.TestCase;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

public class LocalParserTest {
  public void testCreateJSONArray() throws Exception {
    String array = "[12, \"a\", true]";
    assertEquals(12, LocalParser.create_JSON_Array(array).get(0));
    assertEquals("a", LocalParser.create_JSON_Array(array).get(1));
    assertTrue((Boolean) LocalParser.create_JSON_Array(array).get(2));

    String bad_array = "[12, \"]\"";
    boolean catched_the_exception = false;
    try {
      LocalParser.create_JSON_Array(bad_array);
    }
    catch (IllegalArgumentException e) { catched_the_exception = true; }
    assertTrue(catched_the_exception);
  }
}
