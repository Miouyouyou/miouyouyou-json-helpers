package com.miouyouyou.libraries.json;

import org.json.JSONArray;

import com.miouyouyou.libraries.json.JSONArrayToArray;

import junit.framework.Assert;
import junit.framework.TestCase;

public class JSONArrayToArrayTest extends TestCase {
  public void testSimpleArrays() throws Exception {
    JSONArray json_array = 
      new JSONArray("[[[1,2],[\"a\",\"b\"]], 1, 7, \"a\", \"abc\", true,"+
                    "false, null, 78489789485, 2e11, -1654, 9007199254740993]");

    /* Number of expected values in the extracted arrays */
    int strings        = 2;
    int characters     = 1;
    int null_values    = 1;
    int boolean_values = 2;
    int numeric_values = 6;
    int byte_values    = 2;
    int short_values   = 3;
    int int_values     = 3;
    int long_values    = 6;
    int float_values   = 6;
    int double_values  = 6;

    boolean[] boolean_array = 
      (boolean[]) new JSONArrayToArray(boolean.class).extract_values(json_array);
    Boolean[] Boolean_array =
      (Boolean[]) new JSONArrayToArray(Boolean.class).extract_values(json_array);
    Void[] void_array =
      (Void[]) new JSONArrayToArray(Void.class).extract_values(json_array);

    char[] char_array =
      (char[]) new JSONArrayToArray(char.class).extract_values(json_array);
    Character[] Character_array =
      (Character[]) new JSONArrayToArray(Character.class).extract_values(json_array);

    double[] double_array =
      (double[]) new JSONArrayToArray(double.class).extract_values(json_array);
    Double[] Double_array =
      (Double[]) new JSONArrayToArray(Double.class).extract_values(json_array);
    float[] float_array =
      (float[]) new JSONArrayToArray(float.class).extract_values(json_array);
    Float[] Float_array =
      (Float[]) new JSONArrayToArray(Float.class).extract_values(json_array);
    long[] long_array =
      (long[]) new JSONArrayToArray(long.class).extract_values(json_array);
    Long[] Long_array =
      (Long[]) new JSONArrayToArray(Long.class).extract_values(json_array);
    int[] int_array =
      (int[]) new JSONArrayToArray(int.class).extract_values(json_array);
    Integer[] Integer_array =
      (Integer[]) new JSONArrayToArray(Integer.class).extract_values(json_array);
    short[] short_array =
      (short[]) new JSONArrayToArray(short.class).extract_values(json_array);
    Short[] Short_array =
      (Short[]) new JSONArrayToArray(Short.class).extract_values(json_array);
    byte[] byte_array =
      (byte[]) new JSONArrayToArray(byte.class).extract_values(json_array);
    Byte[] Byte_array =
      (Byte[]) new JSONArrayToArray(Byte.class).extract_values(json_array);

    String[] string_array =
      (String[]) new JSONArrayToArray(String.class).extract_values(json_array);

    Assert.assertEquals(boolean_values, boolean_array.length);
    Assert.assertEquals(boolean_values, Boolean_array.length);
    Assert.assertEquals(null_values, void_array.length);
    Assert.assertEquals(characters, char_array.length);
    Assert.assertEquals(characters, Character_array.length);
    
    Assert.assertEquals(double_values, double_array.length);
    Assert.assertEquals(double_values, Double_array.length);
    Assert.assertEquals(float_values, float_array.length);
    Assert.assertEquals(float_values, Float_array.length);
    Assert.assertEquals(long_values, long_array.length);
    Assert.assertEquals(long_values, Long_array.length);
    Assert.assertEquals(int_values, int_array.length);
    Assert.assertEquals(int_values, Integer_array.length);
    Assert.assertEquals(short_values, short_array.length);
    Assert.assertEquals(short_values, Short_array.length);
    Assert.assertEquals(byte_values, byte_array.length);
    Assert.assertEquals(byte_values, Byte_array.length);

    Assert.assertEquals(strings, string_array.length);

  }
}
