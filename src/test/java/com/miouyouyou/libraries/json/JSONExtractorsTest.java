package com.miouyouyou.libraries.json;

import com.miouyouyou.libraries.json.extractors.JSONBooleanExtractor;
import com.miouyouyou.libraries.json.extractors.JSONCharExtractor;
import com.miouyouyou.libraries.json.extractors.JSONCharAndNumberExtractor;
import com.miouyouyou.libraries.json.extractors.JSONClassExtractor;
import com.miouyouyou.libraries.json.extractors.JSONNullExtractor;
import com.miouyouyou.libraries.json.extractors.JSONNumberExtractor;
import com.miouyouyou.libraries.json.extractors.JSONSimpleExtractor;

import java.lang.reflect.Array;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.json.JSONArray;
import org.json.JSONObject;



public class JSONExtractorsTest extends TestCase {
  public void testJSONNullExtractor() throws Exception {
    JSONArray json_array = new JSONArray("[null,[[], null], NULL]");
    Void[] null_array = (Void[]) new JSONNullExtractor().extract_values(json_array);
    Assert.assertEquals(2, null_array.length);
  }

  public void testJSONCharExtractor() throws Exception {
    JSONArray json_array = new JSONArray("[\"s\", [[],\"b\"], null, -7, -7.0, 75, \"fds\", \"\", \"p\"]");
    char[] char_array = (char[]) new JSONCharExtractor(char.class).extract_values(json_array);
    Character[] character_array = (Character[]) new JSONCharExtractor(Character.class).extract_values(json_array);
    Assert.assertEquals(2, char_array.length);
    Assert.assertEquals(2, character_array.length);
    Assert.assertEquals('s', char_array[0]);
    Assert.assertEquals('p', char_array[1]);
    char_array = (char[]) new JSONCharAndNumberExtractor(char.class).extract_values(json_array);
    character_array = (Character[]) new JSONCharAndNumberExtractor(Character.class).extract_values(json_array);
    Assert.assertEquals(3, char_array.length);
    Assert.assertEquals(3, character_array.length);
    Assert.assertEquals('s', char_array[0]);
    Assert.assertEquals((char) 75, char_array[1]);
    Assert.assertTrue((Character) ((char) 75) == char_array[1]);
    Assert.assertEquals('p', char_array[2]);
    char_array = (char[]) new JSONCharAndNumberExtractor(char.class, true, false).extract_values(json_array);
    Assert.assertEquals(5, char_array.length);
    Assert.assertEquals((char) -7, char_array[1]);
    Assert.assertEquals((char) -7.0, char_array[2]);
    char_array = (char[]) new JSONCharAndNumberExtractor(char.class, true, true).extract_values(json_array);
    Assert.assertEquals(5, char_array.length);
    Assert.assertEquals((char) 0, char_array[1]);
    Assert.assertEquals((char) 0, char_array[2]);

    Class[] illegal_arguments = new Class[] {Object.class, int.class, null};
    boolean caught_illegalargumentexception = false;
    for (Class illegal_argument : illegal_arguments) {
      caught_illegalargumentexception = false;
      try {
        new JSONCharExtractor(illegal_argument);
      } catch (IllegalArgumentException e) {
        caught_illegalargumentexception = true;
      }
      Assert.assertTrue(caught_illegalargumentexception);
    }

    for (Class illegal_argument : illegal_arguments) {
      caught_illegalargumentexception = false;
      try {
        new JSONCharAndNumberExtractor(illegal_argument);
      } catch (IllegalArgumentException e) {
        caught_illegalargumentexception = true;
      }
      Assert.assertTrue(caught_illegalargumentexception);
    }
  }

  public void testJSONClassExtractor() throws Exception {
    JSONArray json_array = 
      new JSONArray("[\"int\", \"int[]\", \"java.lang.String\", "+
                    "\"java.lang.String[]\", \"java.lang.reflect.Array\"]");
    Class[] expected_classes =
      new Class[] {int.class, int[].class, String.class, String[].class,
                   java.lang.reflect.Array.class};
    Class[] extracted_classes =
      (Class[]) new JSONClassExtractor().extract_values(json_array);
    for (int index = 0; index < expected_classes.length; index++) {
      Assert.assertEquals(expected_classes[index],
                          extracted_classes[index]);
    }
  }

  public void testJSONSimpleExtrator() throws Exception {
    JSONArray json_array = new JSONArray("[123, 789, 15649841567456, \"a\","+
                                         "1.789]");
    int[] int_array = (int[]) new JSONSimpleExtractor(int.class).extract_values(json_array);
    double[] double_array = (double[]) new JSONSimpleExtractor(double.class).extract_values(json_array);
    String[] string_array = (String[]) new JSONSimpleExtractor(String.class).extract_values(json_array);
    Assert.assertEquals(2, int_array.length);
    Assert.assertEquals(1, double_array.length);
    Assert.assertEquals(1, string_array.length);
  }

  public void testJSONBooleanExtractor() throws Exception {
    JSONArray good_json_array = new JSONArray("[true, True, false]");
    JSONArray bad_json_array  = new JSONArray("[0, 1, \"true\", \"false\", \"#f\"]");
    boolean[] boolean_array = (boolean[]) new JSONBooleanExtractor(boolean.class).extract_values(good_json_array);
    Boolean[] Boolean_array = (Boolean[]) new JSONBooleanExtractor(Boolean.class).extract_values(good_json_array);
    boolean[] empty_array   = (boolean[]) new JSONBooleanExtractor(boolean.class).extract_values(bad_json_array);
    Assert.assertEquals(3, boolean_array.length);
    Assert.assertEquals(3, Boolean_array.length);
    Assert.assertEquals(0, empty_array.length);
    Assert.assertEquals(false, boolean_array[2]);
  }

  public void testJSONNumberExtractor() throws Exception {
    JSONArray json_array = 
      new JSONArray("[123, 9007199254740993, 37000, 15649841567456, \"a\","+
                    "1.789]");
    int[] int_array_no_overflowing_values = 
      (int[]) new JSONNumberExtractor(int.class).extract_values(json_array);
    int[] int_array_overflowing_values_limited =
      (int[]) new JSONNumberExtractor(int.class, false, true).extract_values(json_array);
    int[] int_array_accept_overflows = 
      (int[]) new JSONNumberExtractor(int.class, true, false).extract_values(json_array);
    char[] char_numbers_array_no_overflowing_values =
      (char[]) new JSONNumberExtractor(char.class, false, false, true).extract_values(json_array);
    Integer[] Integer_array_no_overflowing_values =
      (Integer[]) new JSONNumberExtractor(Integer.class).extract_values(json_array);
    Long[] long_array_no_overflowing_values =
      (Long[]) new JSONNumberExtractor(Long.class).extract_values(json_array);

    Assert.assertEquals(3, int_array_no_overflowing_values.length);
    Assert.assertEquals(5, int_array_overflowing_values_limited.length);
    Assert.assertEquals(Integer.MAX_VALUE, int_array_overflowing_values_limited[3]);
    Assert.assertEquals(5, int_array_accept_overflows.length);
    Assert.assertEquals((int) 15649841567456L, int_array_accept_overflows[3]);
    Assert.assertEquals(3, char_numbers_array_no_overflowing_values.length);
    Assert.assertEquals(3, Integer_array_no_overflowing_values.length);

    /* Unless your JVM implements the double type in such a way that values do 
       not denormalize after 53 bits, this will pass. 
       Note how the same values are different or the same, depending on how you
       cast them. */
    Assert.assertTrue(9007199254740993L == (long) long_array_no_overflowing_values[1]);
    Assert.assertTrue(9007199254740992L != (long) long_array_no_overflowing_values[1]);
    Assert.assertTrue(9007199254740993L == (double) long_array_no_overflowing_values[1]);
    Assert.assertTrue(9007199254740992L == (double) long_array_no_overflowing_values[1]);

    boolean catched_illegal_argument_exception = false;
    try {
      new JSONNumberExtractor(char.class);
    } catch (IllegalArgumentException e) {
      catched_illegal_argument_exception = true;
    }
    Assert.assertTrue(catched_illegal_argument_exception);
  }

}
