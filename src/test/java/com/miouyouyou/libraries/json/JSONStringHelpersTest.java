package tests.com.miouyouyou.libraries.json;

import static com.miouyouyou.libraries.json.JSONStringHelpers.escape_for_JSON;

import org.json.JSONObject;

import junit.framework.TestCase;
import static junit.framework.Assert.assertEquals;

public class JSONStringHelpersTest extends TestCase {

  public void testEscapeForJSON() throws Exception {
    char[] characters_to_escape = new char[]
      {0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xa, 0xb, 0xc, 0xd, 
       0xe, 0xf, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19,
       0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f, '"', '\\'};

    String non_escaped_string = new String(characters_to_escape);
    String escaped_string = escape_for_JSON(non_escaped_string);
    JSONObject json_object_with_escaped_string =
      new JSONObject(String.format("{\"value\":\"%s\"}", escaped_string));
    String retrieved_string = 
      json_object_with_escaped_string.optString("value");
    
 
   /* This will ease the debugging process. Printing a string with 
       such values on a screen will NOT help. */
    for (int index = 0; index < non_escaped_string.length(); index++) {
      assertEquals(non_escaped_string.charAt(index),
                   retrieved_string.charAt(index));
    }

    /* This problem could be caused by the JSON Parser too */
    assertEquals(non_escaped_string.length(), 
                 retrieved_string.length());
    
  }

}
