package com.miouyouyou.libraries.json.interfaces;

import org.json.JSONArray;

/** The interface is to be implemented by objects extracting appropriate values
 *  from JSON Array objects and returning an Object, containing those 
 *  appropriate values.
 *
 * What 'appropriate values' are extracted and stored in the returned Object,
 * depend of the implementation. The class of the Object returned is also
 * implementation dependant.
 *
 * If returned Objects have component types, e.g. native Array objects, 
 * component_type() should return the Class representing the component type. If
 * such concept do not apply in the implementation, component_type should return
 * JSONArraysExtractor.no_component_type.
 * The presence of this method is due JSONArraysExtractor being first designed
 * to be implemented by objects extracting values from JSON Array and
 * returning them in native Array objects. But when designing objects like
 * JSONNestedArraysToArray that used these extractors to build multidimensional
 * native array, knowing the component type of the arrays returned before-hand
 * was necessary to build the array dimensions.
 *
 */
public interface JSONArraysExtractor {
  
  /** Special properties returned by some methods. */
  public enum NoSuchProperty { COMPONENT_TYPE }

  /** If the Object returned by extract_values methods does not have a component
   *  type, component_type() should return this variable.
   *  Conversely, methods should check if component_type() return this variable
   *  to know if the returned object has a component type. 
   */
  public Class no_component_type =
    NoSuchProperty.COMPONENT_TYPE.getClass();

  /** Extract the values from the provided JSONArray and return an Object 
   *  containing the values deemed appropriate.
   *
   * If the returned object have a component type, e.g. if the returned 
   * objects is a native Array, component_type() will return the class 
   * representing such component type.
   *
   * The values extracted, the type of the returned Object the component-type
   * of the array is defined by the implementation.
   *
   * @param json_array  The JSON Array to extract values from.
   * @return a native Java Array object, containing values deemed appropriate 
   *         by the implementation. The component type of this Array is
   *         defined by the implementation.
   */
  public Object extract_values(JSONArray json_array);
   
  /** Extract the values from the provided String representing a JSON Array,
   *  and return an Object containing the values deemed appropriate.
   *
   * If the returned object have a component type, e.g. if the returned 
   * objects is a native Array, component_type() will return the class 
   * representing such component type.
   *
   * The values extracted, the type of the returned Object the component-type
   * of the array is defined by the implementation.
   *
   * @param json_array  The JSON Array to extract values from.
   * @return a native Java Array object, containing values deemed appropriate 
   *         by the implementation. The component type of this Array is
   *         defined by the implementation.
   */
  public Object extract_values(String json_array);
   
  /** Return the component type of the Object returned by successful execution
   *  of extract_values.
   * @return the component type of the Object returned by successful execution
   *         of extract_values, which is defined by the implementation. If no
   *         such concept apply in the implementation, no_component_type must
   *         be returned instead.
   */
  public Class component_type();
}
