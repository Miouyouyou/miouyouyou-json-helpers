package com.miouyouyou.libraries.json;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

/** Provide methods to create JSON Array Objects, from Strings,
 *  using the JSON Parser used by this library.
 */
public class LocalParser {
  public static JSONArray create_JSON_Array(String json_array_string) {
    try {
      return new JSONArray(json_array_string);
    }
    catch (JSONException e) {
      String error_message =
        String.format("The local JSON Parser could not parse this Array :\n"+
                      "%s", json_array_string);
      throw new IllegalArgumentException(json_array_string, e);
    }
  }  
}

