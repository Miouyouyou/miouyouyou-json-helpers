package com.miouyouyou.libraries.json;

import org.json.JSONArray;
import org.json.JSONException;

import com.miouyouyou.libraries.json.LocalParser;
import com.miouyouyou.libraries.json.interfaces.JSONArraysExtractor;
// Extractors proxied by JSONArrayToArray(Class)
import com.miouyouyou.libraries.json.extractors.JSONNullExtractor;
import com.miouyouyou.libraries.json.extractors.JSONBooleanExtractor;
import com.miouyouyou.libraries.json.extractors.JSONCharExtractor;
import com.miouyouyou.libraries.json.extractors.JSONNumberExtractor;
import com.miouyouyou.libraries.json.extractors.JSONClassExtractor;
import com.miouyouyou.libraries.json.extractors.JSONSimpleExtractor;

import static com.miouyouyou.libraries.helpers.JavaClassHelpers.is_null;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.is_void;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.is_char;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.is_boolean;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.is_boxed_primitive;

/** This object is a proxy for the different extractors provided by this 
 *  library. Depending of the class provided in the constructor, the appropriate
 *  extractor will be constructed with the provided class, without any specific 
 *  options.
 *  You can also provide a custom JSONArraysExtractor to proxy instead.
 *
 * If you construct this object with a Class object, extract_values will only 
 * extract the values from the first dimension of the JSON Array provided, and 
 * return a single-dimension native Array object, which component type is the 
 * Class passed during the construction of this object.<br />
 * That means that JSONArrayToArray(int.class) will return int[] objects, using
 * the JSONNumberExtractor, for example.<br />
 * In its current state, passing classes other than primitive types, boxed 
 * primitive classes and String.class will not provide expected results.
 * 
 * When constructing this object with a custom JSONArraysExtractor, values
 * returned by the proxied methods are not enforced and, therefore, depend of 
 * their implementation.
 *
 * If you plan to convert nested JSON Array objects to multidimensional 
 * native Array objects, see JSONNestedArraysToArray.
 * 
 * Example :
 * <pre>{@code
 * import com.miouyouyou.libraries.json.JSONArrayToArray;
 * import com.miouyouyou.libraries.json.interfaces.JSONArraysExtractor;
 * 
 * public class Test {
 *   public static void main(String[] args) {
 *     String first_array  = "[145, 11897.75e10, \"hello\", null, true]";
 *     String second_array = "[\"Hi\", false, 126.4, -7894, null, null]";
 *
 *     JSONArraysExtractor int_extractor = new JSONArrayToArray(int.class);
 *     JSONArraysExtractor Boolean_extractor =
 *       new JSONArrayToArray(Boolean.class);
 *
 *     int[] int_values_from_first_array = 
 *       (int[]) int_extractor.extract_values(first_array);
 *     int[] int_values_from_second_array =
 *       (int[]) int_extractor.extract_values(second_array);
 *     Boolean[] Boolean_values_from_first_array =
 *       (Boolean[]) Boolean_extractor.extract_values(first_array);
 *     Boolean[] Boolean_values_from_second_array =
 *       (Boolean[]) Boolean_extractor.extract_values(second_array);
 *
 *     for (int value : int_values_from_first_array) {
 *       System.out.println(String.format("First int array : %d", value));
 *     }
 *     for (boolean value : Boolean_values_from_first_array) {
 *       System.out.println(String.format("First Boolean array : %b", value));
 *     }
 *     for (int value : int_values_from_second_array) {
 *       System.out.println(String.format("Second int array : %d", value));
 *     }
 *     for (boolean value : Boolean_values_from_second_array) {
 *       System.out.println(String.format("Second Boolean array : %b", value));
 *     }
 *   }
 * }
 * }</pre>
 * Output : <pre>{@code
 *
 * First int array : 145
 * First Boolean array : true
 * Second int array : 126
 * Second int array : -7894
 * Second Boolean array : false
 * 
 * </pre>} 
 */
public class JSONArrayToArray implements JSONArraysExtractor {

  /** The extractor proxied by this Object */
  private final JSONArraysExtractor extractor;

  /** Proxy a JSONArraysExtractor provided in this library. When provided a 
   *  JSON Array, these extractors will only extract values that are possible
   *  instances of the provided Class.
   * Note that classes that are not primitive types, boxed primitive classes or
   * java.lang.String.class might not provide expected results.
   *
   * @param component_class  determines the extractor proxied.
   *
   * @throws IllegalArgumentException if the provided Class object is null.
   */
  public JSONArrayToArray(Class component_class) {
    if (is_null(component_class)) {
      throw new 
        IllegalArgumentException("Expected a class, got null instead...");
    }

    if (is_void(component_class)) { this.extractor = new JSONNullExtractor(); }
    else if (is_boolean(component_class)) { 
      this.extractor = new JSONBooleanExtractor(component_class);
    } else if (is_char(component_class)) {
      this.extractor = new JSONCharExtractor(component_class);
    } else if (component_class.isPrimitive() || 
               is_boxed_primitive(component_class)) {
      this.extractor = new JSONNumberExtractor(component_class);
    } else if (component_class == Class.class) {
      this.extractor = new JSONClassExtractor();
    } else {
      this.extractor = new JSONSimpleExtractor(component_class);
    }
  }

  /** Proxy the provided JSONArraysExtractor.
   * @param json_array_extractor  The object implementing the 
   *                              JSONArraysExtractor interface, that will be
   *                              proxied by this object.
   */
  public JSONArrayToArray(JSONArraysExtractor json_array_extractor) {
    this.extractor = json_array_extractor;
  }

  /** Call extract_values(String) on the proxied object and return the returned
   *  value. 
   * @param json_array_string  The JSON Array to extract values from.
   * @return The value returned when calling extract_values(String) on the 
   *         proxied Object. If this object was constructed with a Class object,
   *         the returned value should be a native Array, which component type
   *         is the Class provided during construction, and which values are
   *         possible instance of the class. 
   *         Example : Long.class will return a Long[] array containing only 
   *                   the integer numbers of the provided JSON Array, with no 
   *                   'null' values. 
   */
  public Object extract_values(String json_array_string) {
    return this.extractor.extract_values(json_array_string);
  }

  /** Call extract_values(org.json.JSONArray) on the proxied object and return 
   *  the returned value. 
   * @param json_array  The JSON Array to extract values from.
   * @return The value returned when calling extract_values(org.json.JSONArray)
   *         on the proxied Object. If this object was constructed with a Class
   *         object, the returned value should be a native Array, which 
   *         component type is the Class provided during construction, and which
   *         values are possible instance of the class. 
   *         Example : Long.class will return a Long[] array containing only 
   *                   the integer numbers of the provided JSON Array, with no 
   *                   'null' values. 
   */  
  public Object extract_values(JSONArray json_array) {
    return this.extractor.extract_values(json_array);
  }

  /** If the object returned by the proxied extractor has a component type, 
   *  return the component type of the Object returned by extract_values.
   *
   * For example, if the proxied extractor were to return a String[] object,
   * component_type should return String.class. If the proxied extractor were to
   * return List<String>, component_type should also return String.class.
   * 
   * @return a Class object representing the component type of the object 
   *         returned by the proxied extractor, which might be null if such 
   *         concept do not apply to the objects returned by this extractor.
   */
  public Class component_type() {
    return this.extractor.component_type();
  }
  
}
