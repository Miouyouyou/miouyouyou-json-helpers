package com.miouyouyou.libraries.json.extractors;

import com.miouyouyou.libraries.json.extractors.JSONExtractor;

import java.lang.reflect.Array;
import java.util.List;
import java.util.ArrayList;

/** This object provide methods to copy single character String objects from 
 *  provided JSON Array objects, in char[] or Character[] objects.
 *
 * The main purpose of this library is to convert a JSON Array into a native
 * array, that will be passed to methods working with native arrays, through
 * reflection.
 *
 * Only String containing only one character in the provided JSON Array
 * objects, will be stored in the returned Array object. No 'null' value will
 * be stored.
 *
 * The component type of the returned arrays is defined by the class specified 
 * in the constructor, and is limited to char.class and Character.class. As 
 * stated in multiple extractors documentation, set the component type or the 
 * returned arrays wisely as char[] and Character[] are not compatible objects,
 * even if char values can be casted to Character.
 *
 * Example :
 *<pre>{@code
 * import com.miouyouyou.libraries.json.extractors.JSONCharExtractor;
 * import com.miouyouyou.libraries.json.interfaces.JSONArraysExtractor;
 * 
 * public class Test {
 *   public static void main(String[] args) {
 *     String json_array = "[\"a\", \"b\", 456, null, \"cd\", true, false]";
 *     JSONArraysExtractor extractor = new JSONCharExtractor(char.class);
 *     JSONArraysExtractor Extractor = new JSONCharExtractor(Character.class);
 *     display((char[]) extractor.extract_values(json_array));
 *     display((Character[]) Extractor.extract_values(json_array));
 *     try {
 *       Test.class
 *         .getMethod("display", Character[].class)
 *         .invoke(null, Extractor.extract_values(json_array));
 *     } 
 *     catch (NoSuchMethodException e) { e.printStackTrace(); }
 *     catch (IllegalAccessException e) { e.printStackTrace(); }
 *     catch (java.lang.reflect.InvocationTargetException e) { 
 *       e.printStackTrace(); 
 *     }
 *   }
 *    
 *   public static void display(char[] char_array) {
 *     System.out.println("char[] content : ");
 *     System.out.println(char_array);
 *   }
 * 
 *   public static void display(Character[] character_array) {
 *     System.out.println("Character[] content : ");
 *     for (Character character : character_array) {
 *       System.out.print(character.toString());
 *     }
 *     System.out.println();
 *   }
 * }
 *}</pre>
 *
 * Outputs :
 *<pre>{@code
 * char[] content : 
 * ab
 * Character[] content : 
 * ab
 * Character[] content : 
 * ab
 *}</pre>
 */
public class JSONCharExtractor extends JSONExtractor {

  /** Prepare the extractor by setting the component type of the returned 
   *  array that will store the characters extracted.
   *
   * Remember that char[] and Character[] are not compatible, so set the
   * class wisely.
   *
   * Only strings containing one character will be extracted.
   * /!\ *3-byte and 4-bytes characters will not be extracted correctly* ! 
   * Only the first two bytes will be extracted, since char and Character
   * types can only store 16 bits unsigned values.
   *
   * @param char_class  The component class of the returned array. Either 
   *                    char.class or Character.class.
   * @throws IllegalArgumentException  if char_class isn't char.class or
   *                                   Character.class
   */
  public JSONCharExtractor(Class char_class) {
    if (char_class == null) {
      throw new
        IllegalArgumentException("Expected a class, got null instead...");
    }
    if (char_class != char.class &&
        char_class != Character.class) {
      throw new 
        IllegalArgumentException("Only char.class or Character.class accepted "+
                                 "as first argument. Got this class instead : "+ 
                                 char_class);
    }
    this.component_type = char_class;
  }

  /** Check if the object extracted is a character. If it is, the character
   *  will be stored in the returned Array object.
   *
   * @param value_from_json_array  The object checked.
   * @return true if the value is a single character String, false otherwise.
   */
  protected boolean is_good_value(Object value_from_json_array) {
    return (value_from_json_array instanceof String &&
            ((String) value_from_json_array).length() == 1);
  }

  /** Cast the value that passed the 'is_good_value' check, and add it to the 
   *  list of good values to store in the final Array.
   * @param list  the list of good values to store in the final Array.
   * @param extracted_value  The extracted value that passed the 
   *                         'is_good_value(Object)' check.
   */
  @Override
  protected void add_good_value_to(List<Object> list, 
                                   Object extracted_value) {
    list.add(((String) extracted_value).charAt(0));
  }

  /** Create the final Array that will contain the appropriate values extracted,
   *  and be returned by extract_values. The component type of the Array is
   *  defined during the construction.
   * @param size  The size of the Array. By default, it is equal to the number
   *              of elements stored in the list of good values.
   * 
   */
  protected Object create_array(int size) {
    return Array.newInstance(this.component_type, size);
  }
}
