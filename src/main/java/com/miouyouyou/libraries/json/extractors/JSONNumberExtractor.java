package com.miouyouyou.libraries.json.extractors;

import com.miouyouyou.libraries.json.extractors.JSONExtractor;
import com.miouyouyou.libraries.json.extractors.JSONSimpleExtractor;
import com.miouyouyou.libraries.helpers.PrimitiveBoxedCaster;
import com.miouyouyou.libraries.helpers.QuickBoxedCaster;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Array;
import java.util.List;

/** This object provide methods to copy numbers, limited or not to the 
 *  specified type range, from a JSON Array object, into a native Array of the 
 *  specified class.
 *
 * Example : Constructing this object with int.class will make extract_values
 * return int[] objects. If constructed with Long.class instead, extract_values
 * will generate Long[] objects.
 *
 * By default, only numbers fitting the representable range of the provided
 * number type are copied. Depending on the options provided during the
 * construction of the objects, numbers outside the range of the current class
 * might be casted then copied too.
 *
 * Notes :
 *
 * Primitive and boxed primitive are not compatible types when it comes to 
 * Array. For example, int[] is not compatible with Integer[] and,
 * therefore, cannot be passed to a method expecting an Integer[] object.
 *
 * Long values will not be denormalized.
 *
 * The JSON specification forbid special floating-point values in JSON 
 * structures. Passing a JSON Structure containing Infinity, -Infinity
 * or NaN, without quotes, will trigger an Exception from the parser. Passing
 * a double value that overflows to Infinity will trigger an Exception from
 * the JSON parser used (org.json).
 * This Object will not try to read Strings to determine if they contain words
 * defining special floating-point values, nor will it try to recover from
 * parser errors. 
 * Therefore, special floating-point values will not be extracted with this
 * extractor.
 *
 * Example :
 *<pre>{@code
 * import com.miouyouyou.libraries.json.extractors.JSONNumberExtractor;
 * import com.miouyouyou.libraries.json.interfaces.JSONArraysExtractor;
 * 
 * public class Test {
 *   public static void main(String[] args) {
 *     String json_array = "[127, \"b\", 456, 50, \"cd\", -1000, true, false]";
 *     JSONArraysExtractor extractor = new JSONNumberExtractor(Byte.class);
 *     JSONArraysExtractor overflowing_extractor = 
 *       new JSONNumberExtractor(Byte.class, false, true);
 *     JSONArraysExtractor char_number_extractor =
 *       new JSONNumberExtractor(char.class, false, true, true);
 * 
 *     display((Byte[]) extractor.extract_values(json_array));
 *     display((Byte[]) overflowing_extractor.extract_values(json_array));
 *     display((char[]) char_number_extractor.extract_values(json_array));
 *     try {
 *       Test.class
 *         .getMethod("display", Byte[].class)
 *         .invoke(null, overflowing_extractor.extract_values(json_array));
 *     } 
 *     catch (NoSuchMethodException e) { e.printStackTrace(); }
 *     catch (IllegalAccessException e) { e.printStackTrace(); }
 *     catch (java.lang.reflect.InvocationTargetException e) { 
 *       e.printStackTrace(); 
 *     }
 *   }
 *    
 *   public static void display(Byte[] Byte_array) {
 *     System.out.println("byte[] content : ");
 *     for (byte byte_value : Byte_array) {
 *       System.out.print(String.format("[%d] ", byte_value));
 *     }
 *     System.out.println();
 *   }
 * 
 *   public static void display(char[] char_array) {
 *     System.out.println("char[] content : ");
 *     for (char character : char_array) {
 *       System.out.print(String.format("[%c]", character));
 *     }
 *     System.out.println();
 *   }
 * }
 *}</pre>
 *
 * Outputs :
 *<pre>{@code
 * byte[] content : 
 * [127] [50] 
 * byte[] content : 
 * [127] [127] [50] [-128] 
 * char[] content : 
 * [][ǈ][2][]
 * byte[] content : 
 * [127] [127] [50] [-128] 
 *</pre>}
 */
public class JSONNumberExtractor extends JSONExtractor {

  /** Used to cast values if desired */
  private PrimitiveBoxedCaster caster;

  /** Prepare the extractor by setting the component type of the returned
   *  Array object.
   *
   * When prepared with this constructor, the extractor will only copy values
   * fitting the range of the primitive or boxed primitive number class 
   * provided. If the class represent integer values, floating-point values 
   * fitting the range will be copied truncated. <br />
   * Example : When passing int.class or Integer.class, only values between 
   *           -2^31 and 2^31-1 will be stored in the returned Array objects.
   *
   * char.class cannot be passed to this constructor and will trigger an
   * IllegalArgumentException.
   *
   * Note :
   *
   * Primitive typed Array and boxed primitive typed Array are not compatible.
   * For example, long[] is not compatible with Long[], therefore, long[] 
   * cannot be passed to methods expecting an Long[] object. Set the class of
   * returned Array wisely.
   *
   * @param component_class  The component class of the returned array, which
   *                         must be a primitive or boxed primitive number 
   *                         class.
   *
   * @throws IllegalArgumentException  if the Class object provided is null, the
   *                                   class provided is char.class or 
   *                                   Character.class; or if the provided class
   *                                   is not a primitive or boxed primitive 
   *                                   class representing numbers.
   */
  public JSONNumberExtractor(Class component_class) {
    this(component_class, false, false, false);
  }


  /** Prepare the extractor by setting the component type of the returned
   *  Array object, and how to manage overflowing numbers.
   *
   * When prepared with this constructor, if both boolean are false, the 
   * extractor will only copy values fitting the range of the primitive or 
   * boxed primitive number class provided. If the class represent integer 
   * values, floating-point values fitting the range will be copied truncated. 
   * <br />
   * Example : When passing int.class or Integer.class as first argument, only 
   *           values between -2^31 and 2^31-1 will be stored in the returned 
   *           Array objects. Floating-point values will be truncated.
   *
   * If standard_cast_overflow is set, overflowing values will be casted 
   * normally during extraction, then copied in the final Array objects. 
   * Setting this flag disables the limit_overflow_to_minmax flag. 
   * <br />
   * Example : When passing int.class or Integer.class and setting
   *           standard_cast_overflow to true, integer values fitting the 
   *           [-2^31,(-2^31)-1] range will be stored as is, other values will 
   *           be casted to (int) then stored in the Array object.
   *
   * If limit_overflow_to_minmax is set, however, overflowing values will be 
   * extracted and set to the nearest representable value of the provided number
   * class, before being stored in the final Array objects.
   * If the class provided represent integer values, floating-point values will
   * be stored truncated. <br />
   * Example : When passing int.class or Integer.class as first argument, and 
   *           setting limit_overflow_to_minmax to true, integer values fitting 
   *           the [-2^31,(-2^31)-1] range will be stored as is, values lower 
   *           than -2^31 will be stored as -2^31, and values higher than 2^31-1
   *           will be stored as 2^31-1 in the returned Array objects. Floating-
   *           point values will be truncated.
   *
   * char.class cannot be passed to this constructor and will trigger an
   * IllegalArgumentException.
   *
   * @param component_class  The component class of the returned array, which
   *                         must be a primitive or boxed primitive number 
   *                         class.
   * @param standard_cast_overflow  Determines if overflowing numbers should be
   *                                casted and stored.
   *                                The class used to cast will always be a 
   *                                primitive type. 
   *                                Disables limit_overflow_to_minmax.                    
   * @param limit_overflow_to_minmax Determines if overflowing numbers should be
   *                                 set to the nearest number, representable by
   *                                 the provided numeric class, and stored.
   * @throws IllegalArgumentException  if the Class object provided is null, the
   *                                   class provided is char.class or 
   *                                   Character.class; or if the provided class
   *                                   is not a primitive or boxed primitive 
   *                                   class representing numbers.
   */
  public JSONNumberExtractor(Class component_class,
                             boolean standard_cast_overflow,
                             boolean limit_overflow_to_minmax) {
    this(component_class, standard_cast_overflow, 
         limit_overflow_to_minmax, false);
  }

  /** Prepare the extractor by setting the component type of the returned
   *  Array object, and how to manage overflowing numbers.
   *
   * When prepared with this constructor, if both boolean are false, the 
   * extractor will only extract values fitting the range of the primitive or 
   * boxed primitive number class provided. If the class represent integer 
   * values, floating-point values fitting the range will be extracted 
   * truncated. <br />
   * Example : When passing int.class or Integer.class as first argument, only 
   *           values between -2^31 and 2^31-1 will be stored in the returned 
   *           Array objects. Floating-point values will be truncated.
   *
   * If standard_cast_overflow is set, overflowing values will be casted 
   * normally during extraction, then copied in the final Array objects. 
   * Setting this flag disables the limit_overflow_to_minmax flag. 
   * <br />
   * Example : When passing int.class or Integer.class and setting
   *           standard_cast_overflow to true, integer values fitting the 
   *           [-2^31,(-2^31)-1] range will be stored as is, other values will 
   *           be casted to 'int' then stored in the Array object.
   *
   * If limit_overflow_to_minmax is set, however, overflowing values will be 
   * extracted and set to the nearest representable value of the provided number
   * class, before being stored in the final Array objects.
   * If the class provided represent integer values, floating-point values will
   * be stored truncated. <br />
   * Example : When passing int.class or Integer.class as first argument, and 
   *           setting limit_overflow_to_minmax to true, integer values fitting 
   *           the [-2^31,(-2^31)-1] range will be stored as is, values lower 
   *           than -2^31 will be stored as -2^31, and values higher than 2^31-1
   *           will be stored as 2^31-1 in the returned Array objects. 
   *           Floating-point values will be truncated.
   *
   * If the i_just_want_16bits_numbers is set, char.class and Character.class
   * can be passed as component_class. However, only numbers in accordance with
   * the overflowing rules used will be extracted. No single character String
   * will be represented in the returned char[] or Character[] array, when
   * passing char.class or Character.class.
   *
   * @param component_class  The component class of the returned array, which
   *                         must be a primitive or boxed primitive number 
   *                         class.
   * @param standard_cast_overflow  Determines if overflowing numbers should be
   *                                casted and stored.
   *                                The class used to cast will always be a 
   *                                primitive type. 
   *                                Disables limit_overflow_to_minmax.                    
   * @param limit_overflow_to_minmax Determines if overflowing numbers should be
   *                                 set to the nearest number, representable by
   *                                 the provided numeric class, and stored.
   * @param i_just_want_16bits_numbers  Allows char.class or Character.class as
   *                                    component_class. 
   * @throws IllegalArgumentException  if the Class object provided is null, if 
   *                                   i_just_want_16bits_values is not set and
   *                                   the class provided is char.class or 
   *                                   Character.class; or if the provided class
   *                                   is not a primitive or boxed primitive 
   *                                   class representing numbers.
   */
  public JSONNumberExtractor(Class component_class,
                             boolean standard_cast_overflow,
                             boolean limit_overflow_to_minmax,
                             boolean i_just_want_16bits_numbers) {

    /* We only deal with numbers with this class.
       'char' (and its boxed version Character) can be used as unsigned 16 bits
       number storage so we allow them, but only if the appropriate flag is set
       on the constructor ! In that mode, real characters are ignored though...
    */
    if (component_class == null) {
      throw new 
        IllegalArgumentException("Expected a class, got 'null'\n"+
                                 "If you want to catch null values, "+
                                 "use JSONNullExtractor.");
    }

    else if (!i_just_want_16bits_numbers &&
             (component_class == char.class ||
              component_class == Character.class)) {
      throw new
        IllegalArgumentException("JSONCharAndNumberExtractor is more adequate "+
                                 "for extracting chars or Characters from a "+
                                 "JSON array. It can also extract 16 bits "+
                                 "unsigned numbers.\n"+
                                 "If you only want 16 bits unsigned numbers "+
                                 "stored in a char/Character array, enable "+
                                 "the appropriate flag...\n"+
                                 "Look at the documentation for explanations.");
    }

    else if ((!component_class.isPrimitive() && 
              !(Number.class.isAssignableFrom(component_class))) ||
             component_class == boolean.class) {
      throw new
        IllegalArgumentException("JSONNumberExtractor require a class that is "+
                                 "either a primitive representing numbers or "+
                                 "a boxed primitive class instance of Number.");
    }

    this.component_type = component_class;

    /* The standard cast setting takes priority on the limited overflow setting.
       Remember, though, that by default, no overflowing is allowed... */
    if (standard_cast_overflow) limit_overflow_to_minmax = false;
    /* The flag 'no overflow' will be set if no specific overflowing method is
       set */
    boolean no_overflow = (!standard_cast_overflow &&
                           !limit_overflow_to_minmax);
    /* Quick box constructor works take the following parameters :
       - The array component class (Class)
       - Limit overflows to min/max (boolean)
       - Throw a NumberFormatException on overflow (boolean) */
    this.caster                = 
      new QuickBoxedCaster(component_class, 
                           limit_overflow_to_minmax, 
                           no_overflow);
  }

  /** Determine if the extract value could be added in the final Array.
   * Currently, only check if the value is a Number.
   * @param value_from_json_array  extracted value checked.
   * @return true if the extracted value is a number, false otherwise.
   */
  protected boolean is_good_value(Object value_from_json_array) {
    return (value_from_json_array instanceof Number);
  }

  /** Add the value to the list of good values, in accordance to rules
   *  defined during the construction of the object.
   * Example : overflowing values will not be added if overflowing is
   *           not allowed during the construction of this object.
   *
   * @param list  the list of good values to store in the final Array.
   * @param extracted_value  the value extracted from the JSON Array,
   *                         previously checked in 'is_good_value'.
   */
  protected void add_good_value_to(List<Object> list, 
                                   Object extracted_value) {
    /* If you're like : WHY NOT POLYMORPHISM, that's NOT possible for
       multiple reasons.
       * I will not implement cast(Number number) on a generic casting
       library, as it would slow things down even more.
       * I know that this JSON library has methods like getLong and getDouble,
       but it tries to coerce the value before.
       Recasting a coerced value is equivalent to double casting, which leads
       to two problems :
       1 - If the value was a floating-point number that is beyond the limits
       of the coerced integer type, it will be set to the nearest maximal
       value representable. Then, if you recast this coerced value to a
       a lower integer type that cannot hold this value, the value will 
       wrap around the limits of the integer type, leading to an 
       unexpected result.
       Example :
       Direct cast to int of the value 18446744073709555712.0
       -> 2147483647
       Double cast to long then int of the same value
       -> -1
       2 - If the value is blindly casted to double, no matter what, you'll
       lose precision for some values between |2^53| and |2^63-1| bits.

       If you wonder why I'm only checking if the value is a Double, Long or
       Integer, it's because they are the only number classes returned by the 
       method 'JSONArray#get' when extracting a number.
    */
    try {
      if (extracted_value instanceof Double) {
        list.add(caster.cast((Double) extracted_value));
      } 
      /* You can't do (long) Object cast in Java because the compiler is too 
         dumb to understand the Object's class, even after an 
         'instanceof <final class>' check... */
      else if (extracted_value instanceof Long) {
        list.add(caster.cast((Long) extracted_value));
      } else if (extracted_value instanceof Integer) {
        list.add(caster.cast((Integer) extracted_value));
      }
    } catch (NumberFormatException e) {}
  }

  /** Create the Array that will be returned by extract_values. 
   * The component type of the created Array is the component class provided
   * during the construction of this object.
   * @param size  The size of the array, equivalent to the number of elements
   *              added in the list of good values.
   * @return an Array of the provided size, which component type is the Class
   *         provided during the construction of this object.
   */
  protected Object create_array(int size) {
    return Array.newInstance(component_type, size);
  }

}
