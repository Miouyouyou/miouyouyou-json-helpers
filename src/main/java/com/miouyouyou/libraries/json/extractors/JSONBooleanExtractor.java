package com.miouyouyou.libraries.json.extractors;

import com.miouyouyou.libraries.json.extractors.JSONExtractor;
import com.miouyouyou.libraries.helpers.PrimitiveBoxedCaster;
import com.miouyouyou.libraries.helpers.QuickBoxedCaster;

import org.json.JSONArray;

import java.lang.reflect.Array;
import java.util.List;

/** This object provide methods to copy boolean values from JSON
 *  Array objects, into boolean[] or Boolean[].
 *
 * The main purpose of this library is to convert a JSON Array into a native
 * array, that will be passed to methods working with native arrays, through
 * reflection.
 *
 * This object only copy boolean values. **No 'null' value will be copied** in 
 * Boolean[] array.
 *
 * The component type of the returned arrays is defined by the class specified 
 * in the constructor, and is limited to boolean.class and Boolean.class. As 
 * stated in multiple extractors documentation, set the component type or the 
 * returned arrays wisely as boolean[] and Boolean[] are not compatible objects,
 * even if boolean values can be casted to Boolean.
 *
 * Example :
 * <pre>{@code
 * import com.miouyouyou.libraries.json.extractors.JSONExtractor;
 * import com.miouyouyou.libraries.json.extractors.JSONBooleanExtractor;
 * 
 * public class Test {
 *   public static void poll_result(Boolean[] results) {
 *     int yes = 0, no = 0;
 *     for (Boolean result : results) {
 *       if (result) { yes++; }
 *       else { no++; }
 *     }
 *     String displayed_result = 
 *       String.format("%d Yes / %d No (%d Results)", yes, no, results.length);
 *     System.out.println(displayed_result);
 *   }
 *
 *   public static void poll_result(boolean[] results) {
 *     int yes = 0, no = 0;
 *     for (boolean result : results) {
 *       if (result) { yes++; }
 *       else { no++; }
 *     }
 *     String displayed_result = 
 *       String.format("%d yes / %d no (%d results)", yes, no, results.length);
 *     System.out.println(displayed_result);
 *   }
 *
 *   public static void main(String[] args) {
 *     String json_array = "[true, true, false, \"I don't know\"]";
 *     JSONExtractor extractor = new JSONBooleanExtractor(boolean.class);
 *     JSONExtractor Extractor = new JSONBooleanExtractor(Boolean.class);
 *
 *     poll_result((boolean[]) extractor.extract_values(json_array));
 *     poll_result((Boolean[]) Extractor.extract_values(json_array));
 *  
 *     // No need to cast if you pass the extracted array to an extracted method
 *     try {
 *      Test.class
 *        .getMethod("poll_result", boolean[].class)
 *        .invoke(null, extractor.extract_values(json_array));
 *     } 
 *     catch (NoSuchMethodException e) { e.printStackTrace(); }
 *     catch (IllegalAccessException e) { e.printStackTrace(); }
 *     catch (java.lang.reflect.InvocationTargetException e) { 
 *       e.printStackTrace(); 
 *     }
 *   }
 * 
 * }
 * }</pre>
 *
 * Outputs : 
 *<pre>{@code
 * 2 yes / 1 no (3 results)
 * 2 Yes / 1 No (3 Results)
 * 2 yes / 1 no (3 results)
 *}</pre>
 */

public class JSONBooleanExtractor extends JSONExtractor {

  /** Prepare the extractor, by setting the desired component class, 
   *  boolean.class or Boolean.class.
   *
   * Remember that boolean[] and Boolean[] are NOT compatible.
   * If you provide boolean.class, a boolean[] will be created, and
   * if you provide Boolean.class, a Boolean[] will be created instead.
   *
   * Other values will trigger an IllegalArgumentException.
   *
   * Currently, this class only copy values that are real booleans. No 'null' 
   * values will be copied in the final Array objects.
   *
   * @param boolean_class  The component type of the Array objects returned. 
   * @throws IllegalArgumentException if boolean_class isn't a boolean
   *                                  class.
   */
  public JSONBooleanExtractor(Class boolean_class) {
    if (boolean_class != boolean.class &&
        boolean_class != Boolean.class) {
      throw new 
        IllegalArgumentException("Only boolean.class and Boolean.class "+
                                 "are accepted.");
    }
    this.component_type = boolean_class;
  }

  protected Object create_array(int size) {
    return Array.newInstance(this.component_type, size);
  }

  protected boolean is_good_value(Object value_from_json_array) {
    return (value_from_json_array instanceof Boolean);
  }

}
