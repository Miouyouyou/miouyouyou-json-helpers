package com.miouyouyou.libraries.json.extractors;

import static com.miouyouyou.libraries.helpers.JavaClassHelpers.class_from_literal;
import com.miouyouyou.libraries.json.extractors.JSONExtractor;

import java.lang.reflect.Array;
import java.util.List;

/** This object extract String containing valid class literals, according to
 *  the execution environmnt, from a JSON Array and creates a Class[] objects
 *  containing the classes represented by those literals.
 *
 * Example :
 *<pre>{@code
 * import com.miouyouyou.libraries.json.extractors.JSONClassExtractor;
 * import com.miouyouyou.libraries.json.interfaces.JSONArraysExtractor;
 *  
 * public class Test {
 *   public static void main(String[] args) {
 *     String class_literals = 
 *       "[\"int\", \"short[]\", \"java.lang.Character[][]\", null, \"true\","+
 *       " \"false\", 7894986, \"java.lang.Exception\", \"Hello !\", "+
 *       " \"Integer\"]";
 *     String literal_signature = "[\"char[]\", \"int\", \"int\"]";
 *     JSONArraysExtractor class_extractor = new JSONClassExtractor();
 *     Class[] extracted_classes =
 *       (Class[]) class_extractor.extract_values(class_literals);
 *     for (Class extracted_class : extracted_classes) {
 *       System.out.print(String.format("[%s] ", extracted_class.getName()));
 *     }
 *     System.out.println();
 *  
 *     Class base_class = String.class;
 *     Class[] signature =
 *       (Class[]) class_extractor.extract_values(literal_signature);
 *     char[] char_array = new char[] {'a', 'b', 'c', 'd', 'e'};
 *     try {
 *       // Will output 'bcd'. 
 *       // Currently equivalent to :
 *       // 'System.out.println(new String(char_array, 1, 3))'
 *       System.out.println((String) base_class
 *                          .getConstructor(signature)
 *                          .newInstance(char_array, 1, 3));
 *     }
 *     catch (NoSuchMethodException e)  { e.printStackTrace(); }
 *     catch (IllegalAccessException e) { e.printStackTrace(); }
 *     catch (InstantiationException e) { e.printStackTrace(); }
 *     catch (java.lang.reflect.InvocationTargetException e) {
 *       e.printStackTrace(); 
 *     }
 *     catch (ExceptionInInitializerError e) { e.printStackTrace(); }
 *   }
 * }
 *}</pre>
 *
 * Outputs :
 *<pre>{@code
 * [int] [[S] [[[Ljava.lang.Character;] [java.lang.Exception] 
 * bcd
 *</pre>}
 */
public class JSONClassExtractor extends JSONExtractor {

  /** Construct the extractor */
  public JSONClassExtractor() { this.component_type = Class.class; }

  /** Create the Class[] array that will be returned by extract_values.
   * @param size  size equivalent to the number of valid class literals found 
   *              in the provided JSON Array.
   * @return a Class[] array of the desired size.
   */
  protected Object create_array(int size) {
    return Array.newInstance(component_type, size);
  }

  /** Stores the Class object represented by the current class literal found in
   *  the JSON Array, in the list of good values to store in the final Class[] 
   *  array, if it's a valid class literal.
   *
   *  @param list  the list of good values to store in the final Class[] object.
   *  @param extracted_value  the current value, checked by is_good_value, that
   *                          might represent a valid class literal.
   */
  protected void add_good_value_to(List<Object> list,
                                   Object extracted_value) {
    try {
      list.add(class_from_literal((String) extracted_value));
    } catch (ClassNotFoundException e) {}
  }

  /** Checks if the current value, in the JSON Array, is a String.
   * @param value_from_json_array  the current value checked.
   * @return true if the value is a String, false otherwise.
   */
  protected boolean is_good_value(Object value_from_json_array) {
    return (value_from_json_array instanceof String);
  }
}
