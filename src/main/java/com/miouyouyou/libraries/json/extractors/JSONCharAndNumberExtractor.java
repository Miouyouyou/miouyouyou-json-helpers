package com.miouyouyou.libraries.json.extractors;

import com.miouyouyou.libraries.json.extractors.JSONExtractor;
import com.miouyouyou.libraries.helpers.QuickBoxedCaster;
import com.miouyouyou.libraries.helpers.PrimitiveBoxedCaster;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.is_decimal_Number;

import java.lang.reflect.Array;

import java.util.List;
import java.util.ArrayList;

/** This object provide methods to copy single characters and numbers 
 *  (limited or not to the unsigned 16 bits range) from JSON Array objects,
 *  in char[] or Character[] Array objects. 
 *  The JSON Array objects provided are not modified.
 *
 * The main purpose of this library is to convert a JSON Array into a native
 * array, that will be passed to methods working with native arrays, through
 * reflection.
 *
 * By default, only numbers fitting in the [0,65535] range are copied.
 * Floating-point values will be also copy, though they will be truncated.
 * Numbers outside this range can also be casted to char, and some control
 * is offered when casting overflowing numbers.
 *
 * The component type of the returned arrays is defined by the class specified 
 * in the constructor, and is limited to char.class and Character.class. As 
 * stated in multiple extractors documentation, set the component type or the 
 * returned arrays wisely as char[] and Character[] are not compatible objects,
 * even if char values can be casted to Character.
 *
 * Example use :
 *<pre>{@code 
 * import com.miouyouyou.libraries.json.extractors.JSONExtractor;
 * import com.miouyouyou.libraries.json.extractors.JSONCharAndNumberExtractor;
 * 
 * public class Test {
 *  public static void print_chars(Character[] characters) {
 *    if (characters == null || characters.length == 0) return;
 *    for (Character character : characters)
 *      System.out.print(character);
 *    System.out.println();
 *  }
 *
 *  public static void main(String[] args) {
 *    JSONArray json_array = "[8594, \"a\", \"b\", \"c\"]";
 *    JSONExtractor extractor = new JSONCharAndNumberExtractor(Character.class);
 *    // Will print : →abc
 *    print_chars((Character[]) extractor.extract_values(json_array));
 *    // No need to cast if you pass the extracted array to an extracted method
 *    try {
 *     Test.class
 *       .getMethod("print_chars", Character[].class)
 *       .invoke(null, extractor.extract_values(json_array));
 *    } 
 *    catch (NoSuchMethodException e) { e.printStackTrace(); }
 *    catch (IllegalAccessException e) { e.printStackTrace(); }
 *    catch (java.lang.InvocationTargetException e) { 
 *      e.printStackTrace(); 
 *    }
 *  } 
 * }
 *}</pre>
 *
 * Outputs :
 *<pre>{@code
 * →abc
 * →abc
 *}</pre>
 **/
public class JSONCharAndNumberExtractor extends JSONExtractor {

  private boolean
    overflow_is_ok,
    bind_overflows_to_minmax,
    current_value_is_a_number;
  private PrimitiveBoxedCaster
    number_caster;

  /** Prepare the extractor by setting the component type of the returned 
   *  array that will store the characters copied.
   *
   * Remember that char[] and Character[] are not compatible, so set the 
   * class wisely.
   *
   * Strings containing only one character and 16 bits unsigned numbers will
   * be extracted. With this setup, overflows are not allowed so only values
   * between 0 and 65535, inclusive, will also be stored in the Array.
   *
   * /!\ *Surrogate characters will not be extracted correctly* !
   * Only the high-surrogate will be extracted !
   *
   * @param char_class  The component class of the returned array. Either
   *                    char.class or Character.class.
   * @throws IllegalArgumentException  if char_class isn't a character class
   *                                   (char.class or Character.class) or null.
   */
  public JSONCharAndNumberExtractor(Class char_class) {
    this(char_class, false, false);
  }

  /** Prepare the extractor by : setting the component type, determining if
   *  values overflowing this range are to be allowed, and if values overflowing
   *  are to be set to the nearest value representable by a 'char'.
   *
   *  Remember that char[] and Character[] are not compatible, so set the class 
   *  wisely.
   *  Strings containing only one character and 16 bits unsigned numbers will be 
   *  stored. Then, depending on the flags sets : 
   *  - values outside this range will also be stored, minus an overflow which
   *    result depend on the casted value type (integer or floating-point);
   *  - values outside this range will be stored but will be set to the nearest
   *    limit (0 or 65535).
   * /!\ *Surrogate characters will not be extracted correctly* !
   * Only the high-surrogate will be extracted !
   * @param char_class  The component class of the returned array. Either
   *                    char.class or Character.class.
   * @param standard_cast_overflow  Determines if overflowing values should be 
   *                                casted to (char) and stored in the returned
   *                                Array objects. 
   *                                Disables limit_overflow_to_minmax.
   * @param limit_overflow_to_minmax  Determines if overflowing values should be
   *                                  set to the nearest representable number. 
   *                                  (0 or 65535)
   * @throws IllegalArgumentException  if char_class isn't a character class,
   *                                   (char.class or Character.class) or null.
   */
  public JSONCharAndNumberExtractor(Class char_class,
                                    boolean standard_cast_overflow,
                                    boolean limit_overflow_to_minmax) {
    if (char_class != char.class &&
        char_class != Character.class) {
      throw new 
        IllegalArgumentException("Only char.class or Character.class accepted "+
                                 "as first argument. Got : " + char_class +
                                 ".class");
    }
    this.component_type = char_class;

    // The default behaviour of QuickBoxedCaster is to allow overflows.
    boolean deny_overflow = !standard_cast_overflow;
    this.number_caster   = new QuickBoxedCaster(char_class, 
                                                limit_overflow_to_minmax, 
                                                deny_overflow);
    this.current_value_is_a_number = false;
  }

  /** Check if current value extracted from the provided JSONArray is an
   *  appropriate value.
   * @param value_from_json_array  The value to check
   * @return true if the value is a Number or a single char String, 
   *         false otherwise. Note that String containing only one surrogate
   *         character are also accepted, as they are valid 'char' values.
   */
  protected boolean is_good_value(Object value_from_json_array) {
    if (value_from_json_array instanceof Number) {
      this.current_value_is_a_number = true;
      return true;
    } else {
      this.current_value_is_a_number = false;
      return (value_from_json_array instanceof String &&
              ((String) value_from_json_array).length() == 1);
    }
  }

  @Override
  protected void add_good_value_to(List<Object> list,
                                   Object extracted_value) {
    if (this.current_value_is_a_number) {
      if (!is_decimal_Number(extracted_value)) {
        try {
          list.add
            (this.number_caster.cast(((Number) extracted_value).longValue()));
        } catch (NumberFormatException e) {}
      } else {
        try {
          list.add
            (this.number_caster.cast(((Number) extracted_value).doubleValue()));
        } catch (NumberFormatException e) {}
      }
    } else {
      list.add(((String) extracted_value).charAt(0));
    }
  }

  /** Creates the array that will be populated with the extracted values and 
   *  returned.
   * @param size  The size of the Array
   * @return an Array, which component class was defined during the construction
   *         of the object.
   */
  protected Object create_array(int size) {
    return Array.newInstance(component_type, size);
  }

}

