package com.miouyouyou.libraries.json.extractors;

import com.miouyouyou.libraries.json.extractors.JSONExtractor;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.get_boxed_equivalent_of;

import java.lang.reflect.Array;

/** This object is deeply linked to the JSON parser used, org.json, and provide 
 *  methods to copy values that are instance of the class provided when 
 *  constructing the object, in native Array objects. 
 *  The class of the extracted values is determined by JSONArray#get.
 *
 * This class use the org.json.JSONArray#get method to extract values from the 
 * JSON Array provided. Since JSONArray#get only return values as Boolean, 
 * Integer, Long, String, JSONObject or JSONArray objects, extract_values() will
 * return empty arrays if any other class is passed during construction of this
 * object.
 *
 * If you pass a primitive type, the boxed primitive type will be used during
 * checks.
 *
 * Example :
 * <pre>{@code
 * import com.miouyouyou.libraries.json.extractors.JSONSimpleExtractor;
 * import com.miouyouyou.libraries.json.interfaces.JSONArraysExtractor;
 *
 * public class Test {
 *   public static void main(String[] args) {
 *
 *     String json_array = "[true, false, \"b\", 456, 50, \"cd\", -1000, null]";
 *     JSONArraysExtractor string_extractor = 
 *       new JSONSimpleExtractor(String.class);
 *     // All the values in the json_array will be returned as Integer so this
 *     // extractor will return empty arrays.
 *     JSONArraysExtractor useless_extractor = 
 *       new JSONSimpleExtractor(Long.class);
 *
 *     String[] strings = 
 *       (String[]) string_extractor.extract_values(json_array);
 *     // Will display "bcd"
 *     System.out.println(new StringBuilder(strings).toString());
 *     Long[] empty_array =
 *       (Long[]) useless_extractor.extract_values(json_array);
 *     System.out.println("empty array length : %d", empty_array.length);
 *     
 *   }
 * }
 * }</pre>
 */
public class JSONSimpleExtractor extends JSONExtractor {

  /* JSONArray.get always return Object-typed values. So if we want to store 
     the values in a primitive typed Array, we need to know the equivalent 
     boxed class of this primitive type. */
  public final Class boxed_equivalent_class; 

  /** Prepare the extractor by setting the component type of the returned
   *  Array object, and determine what kind of values will be stored.
   * 
   * @param component_class  The class of the values that should be stored in
   *                         returned Array objects, and the component type of
   *                         those Array objects.
   * @throws IllegalArgumentException if provided Class object is null.
   */
  public JSONSimpleExtractor(Class component_class) {
    if (component_class == null) {
      throw new IllegalArgumentException
        ("Please use JSONNullExtractor if you want to extract null values\n");
    }
    this.component_type = component_class;

    if (!this.component_type.isPrimitive()) {
      this.boxed_equivalent_class = this.component_type;
    } else {
      this.boxed_equivalent_class = 
        get_boxed_equivalent_of(this.component_type);
    }

  }
  
  /** Checks if the class of the extracted value matches the class provided
   *  during construction.
   *
   * If a primitive type was passed during the construction of the object,
   * checks are done using the boxed primitive class representing this
   * primitive type.
   * 
   * See org.json.JSONArray#get to see what kind of classes can be matched.
   *
   * @param value_from_json_array  the extracted value checked.
   * @return true if the extracted value's class matches the class provided
   *         during construction, or the boxed primitive equivalent of the
   *         primitive type provided in the constructor; or false otherwise.
   */
  @SuppressWarnings("unchecked")
  protected boolean is_good_value(Object value_from_json_array) {
    return 
      this.boxed_equivalent_class.
      isAssignableFrom((Class<?>) value_from_json_array.getClass());
  }

  /** Create an Array object where good values extracted from the JSON Array 
   *  will be copied, as is.
   * @param array_size  the size of the Array to create, which must be 
   *                    equivalent to the number of good values extracted.
   * @return an Array object. The component type of this Array is the Class
   *         provided during the construction of this object.
   */
  protected Object create_array(int array_size) {
    return Array.newInstance(this.component_type, array_size);
  }


}
