package com.miouyouyou.libraries.json.extractors;

import com.miouyouyou.libraries.json.interfaces.JSONArraysExtractor;
import com.miouyouyou.libraries.json.LocalParser;

import org.json.JSONArray;
import org.json.JSONObject;

/** This extractor return a Void[] array containing all the null values found 
 *  in the JSON Array passed to extract_values.
 */
/* It's a joke meant for people passing void.class or Void.class to 
   JSONArrayToArray(Class). */
public class JSONNullExtractor implements JSONArraysExtractor {

  public JSONNullExtractor() {}

  public Class component_type() {
    return Void.class;
  }

  public Object extract_values(String json_array_string) {
    return extract_values(LocalParser.create_JSON_Array(json_array_string));
  }

  public Object extract_values(JSONArray array) {
    /* Count the number of null values found, create a Void[] array of that
       size, fill it will 'null' values and return the array... 
       Since it return an array with only null values, Void[] seems the most
       appropriate type. */
    Void[] null_array = 
      new Void[number_of_null_values_found_in(array)];
    fill(null_array);
    return null_array;
  }

      
  private int number_of_null_values_found_in(JSONArray array) {
    int null_values = 0;

    for(int index = 0; index < array.length(); index++) {
      try { if (array.get(index) == JSONObject.NULL) null_values++; } 
      catch(Exception e) { continue; }
    }

    return null_values;
  }

  /* There's no Arrays.fill for Void arrays... and nobody cares... */
  private void fill(Void[] null_array) {
    for (int index = 0; index < null_array.length; index++) {
      null_array[index] = null;
    }
  }

}
