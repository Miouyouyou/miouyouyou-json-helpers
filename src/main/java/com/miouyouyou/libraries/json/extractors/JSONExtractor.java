package com.miouyouyou.libraries.json.extractors;

import com.miouyouyou.libraries.json.interfaces.JSONArraysExtractor;
import static com.miouyouyou.libraries.json.LocalParser.create_JSON_Array;

import java.lang.reflect.Array;
import java.util.List;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

/** Abstract class that provides an almost complete implementation of 
 *  JSONArraysExtractor, intended to create native Array from the values stored
 *  in a JSON Array.
 *
 * The values extracted from the JSON Array, provided to extract_values, and 
 * the values stored in the returned Array is, like the component type of those
 * returned native Array objects, implementation-defined.
 *
 * The non-implemented methods are :
 * - is_a_good_value(Object), a boolean method that should return true if the
 *   Object should be stored in the final Array.
 * - create_array(size), a method that return a native Array of the specified 
 *   size, as an Object. The returned Array is intended to be filled with the 
 *   extracted values and then returned to the initial caller.
 *
 * If you want to reimplement some other parts, here's the current execution
 * flow :<pre>{@code
 * extract_values(String) -> extract_values(JSONArray)
 * extract_values(JSONArray json_array)
 * -> extract_good_values(json_array, 
 *                        java.util.List<Object> list)
 *  -> extract_good_value(json_array, int index, list)
 *   -> extract_value(json_array, index)
 *   -> if is_good_value(java.lang.Object extracted_value)
 *    -> add_good_value_to(list, extracted_value)
 * -> return array_created_from(list)
 *  -> create_array(int list_size)
 *  -> store(list, java.lang.Object created_array)
 *  -> return created_array
 * }</pre>
 *
 * The main purpose of this abstract class is to be extended by extractors 
 * returning native Array. However, it is possible to return other kind of 
 * objects, like List or Set.
 *
 */
public abstract class JSONExtractor implements JSONArraysExtractor {

  /** Component type of the Array objects returned by this object. */
  protected Class component_type;

  /** Returns the component type of the Array objects returned by successful 
   *  execution of extract_values.
   *
   * When extract_values return multidimensional Array objects, 
   * component_type() must return the component type of the first dimension.
   * Example :
   * If the extractor return int[] objects, component_type() should return 
   * int.class. If the extract return int[][] objects, component_type() should
   * return int[].class.
   *
   * @return the implementation-defined component type of the Array objects 
   *         returned by successful execution of extract_values.
   */
  public Class component_type() {
    return component_type;
  }

  /** Extract the desired values from the JSON Array represented in the provided 
   *  String, and return an appropriate Array, as Object.
   *
   * @param json_array_string  The String representing the JSON Array to 
   *                           extract the values from.
   * @return An array, which component type is implementation-defined,
   *         containing all the appropriate values.
   * @throws IllegalArgumentException if the provided String cannot be parsed
   *         as a JSON Array by the local parser.
   */
  public Object extract_values(String json_array_string) {
    return extract_values(create_JSON_Array(json_array_string));
  }

  /** Extract the desired values from the JSON Array provided and return an
   *  an appropriate Array, as Object.
   *
   * @param json_array  The JSONArray to extract the values from.
   * @return An array, which component type is implementation-defined, 
   *         containing all the appropriate values.
   */
  public Object extract_values(JSONArray json_array) {
    List<Object> good_values = new ArrayList<Object>();
    extract_good_values(json_array, good_values);
    return array_created_from(good_values);
  }

  /** Used to extract the appropriate values from the JSON Array, and store
   *  them (in a list by default), in order to create the Array afterwards.
   *
   *  List was choosed, since it's a container that can grow dynamically and
   *  can inform on the number of elements stored.
   *
   *  @param json_array  The JSONArray to extract the values from. 
   *  @param list  The list to store extracted values in. 
   */
  protected void extract_good_values(JSONArray json_array,
                                     List<Object> list) {
    //Object extracted_value = null;
    for(int index = 0; index < json_array.length(); index++) {
      extract_good_value(json_array, index, list);
    }
  }


  /** Used to extract a single value from a specific place in the JSON Array.
   *
   * The main purpose of this method, is to separate the extraction logic from
   * the loop logic.
   * The extracted value will be stored in the list supplied as argument.
   *
   * @param json_array  The JSONArray to extract a value from.
   * @param index  The position of the value in the JSONArray.
   * @param list  The list where to store the extract value in.
   */
  /* Note that if you reimplement this method, you'll still have to implement 
     the abstract methods called by this implementation in order to avoid 
     Java compiler error */
  protected void extract_good_value(JSONArray json_array, 
                                    int index, 
                                    List<Object> list) {
    try {
      Object extracted_value = extract_value(json_array, index);
      if (is_good_value(extracted_value))
        add_good_value_to(list, extracted_value);
    } catch (JSONException e) {}
  }

  /** Used to add the extracted value in the temporary list. 
   *
   * If you want to perform actions on the extracted value 
   * before storing it, that is the place. (e.g. : If you want to cast the 
   * good value, this is the place).
   *
   * @param list  The temporary list.
   * @param extracted_value  The value previously extracted. 
   */
  protected void add_good_value_to(List<Object> list, 
                                   Object extracted_value) {
    list.add(extracted_value);
  }

  /** Create the Array with the values stored in the temporary list.
   * @param list  A list containing the values extracted 
   * @return An array, from a type defined by create_array. 
   */
  protected Object array_created_from(List<?> list) {
    Object array = create_array(list.size());
    store(list, array);
    return array;
  }

  /** Utility method used to store the values from a list into 
   *  an Array, whatever the type.
   * ! This method does not check if there is enough place in the Array !
   * By default, this can throw the same RuntimeExceptions that Array.set 
   * throws.
   * @param list  A list containing values to store in an Array.
   * @param array  The array object to store values in.
   */
  protected void store(List<?> list, Object array) {
    for (int index = 0; index < list.size(); index++)
     { Array.set(array, index, list.get(index)); }
  }

  /** Default method for values extraction from a JSON Array.
   *
   * If you want to implement a specific logic for extracting values from a
   * JSON Array, do it here.
   * The default way is to invoke 'org.json.JSONArray#get'
   * @param json_array  The JSON Array to extract a value from.
   * @param index  The positive of the value in the json_array.
   * @return The object extracted.
   * @throws JSONException Conditions of this exception are set by 
   *                       JSONArray#get.
   */
  protected Object  extract_value(JSONArray json_array, int index) 
    throws JSONException {
    return json_array.get(index);
  }

  /** Abstract method that is called to check if the value extracted is an
   *  appropriate value.
   * Mainly used to limit values processed by 'add_good_value_to'.
   * @param value_from_json_array  The value to check
   * @return true if the value is appropriate, false otherwise.
   */
  abstract protected boolean is_good_value(Object value_from_json_array);

  /** Abstract method to create an Array of a determined size. The component
   *  type of the Array depends of the current implementation of this class.
   * @param size  The size of the Array.
   * @return The array, wrapped in an Object.
   */
  abstract protected Object  create_array(int size);

}
