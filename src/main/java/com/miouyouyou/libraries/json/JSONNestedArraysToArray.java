package com.miouyouyou.libraries.json;

import java.lang.reflect.Array;
import java.util.List;
import java.util.ArrayList;

import com.miouyouyou.libraries.json.interfaces.JSONArraysExtractor;
import com.miouyouyou.libraries.json.JSONArrayToArray;
import com.miouyouyou.libraries.json.LocalParser;

import org.json.JSONArray;
import org.json.JSONException;


/** This object converts nested JSON Arrays to native Arrays, using
 *  a JSONArraysExtractor to extract the values of the last dimensions.
 *
 * To convert nested JSON Array objects to a multidimensional Array, pass
 * it to, either : 
 * - to_array(String, JSONArraysExtractor)
 * - to_array(String, Class)
 * or if you use the same JSON library :
 * - to_array(org.json.JSONArray, JSONArraysExtractor)
 * - to_array(org.json.JSONArray, Class)
 *
 * The component type of the last dimension of the resulting Array is
 * determined by the JSONArraysExtractor and its component_type() method. 
 *
 * Remember that multidimensional Array in Java are limited. While each
 * dimension might have different sizes, the component type of each dimension
 * only allow a fixed number of sub-dimensions. Also, concrete values can only
 * be stored in the last dimensions.
 *
 * For example :
 * <pre>{@code
 * public class Test {
 *   public static void main(String[] args) {
 *     int[] single_value = new int[] {1};
 *     int[] seven_values = new int[7];
 *     int[] three_values = new int[] {489, 26789, 23332};
 *
 *     // Correct - storing int[] objects in the first dimension of an int[][]
 *     //           is ok.
 *     int[][] pass = new int[][] {single_value, new int[0],
 *                                 seven_values, three_values};
 *
 *     // Does not compile - storing int[] objects in the last dimension of
 *                           an int[][] is not allowed.
 *     int[][] fail = new int[][] {single_value, new int[0], {seven_values}
 *                                 three_values};
 *
 *     // Does not compile - storing int[][] in the first dimension of an 
 *                           int[][] is not allowed.
 *     int[][] nope = new int[][] {single_value, new int[1][1], three_values};
 *   }
 * }
 * }</pre>
 * Such limitations cannot be sidestepped with reflection :
 *
 * <pre>{@code
 * // This example compiles but throws an Exception on Runtime
 *
 * import java.lang.reflect.Array;
 * 
 * public class Test {
 *   public static void main(String[] args) {
 * 
 *     int[] array = (int[]) Array.newInstance(int.class, 1);
 *     int[][] array_of_array = (int[][]) Array.newInstance(int[].class, 3);
 *     // OK
 *     Array.set(array_of_array, 0, array);       
 *     Array.set(array_of_array, 1, new int[23]);
 *     // EXCEPTION - Array element type mismatch
 *     Array.set(array_of_array, 2, new int[1][1]);
 *   }
 * }
 * }</pre>
 * So, the number of dimensions is fixed in a native Java array and, therefore,
 * arrays cannot be nested like in JSON Array objects.
 * For example :
 * [[1],[[0],[0]], [489, 26789, 23332]] is a valid JSON Array that cannot be
 * mapped as-is to a Java native array.
 * 
 * The method used by this object to parse nested JSON Array objects is to 
 * limit the value extraction to the lowest common number of dimensions found.
 * Meaning that :
 * [1, 2, [1],[[0],[0]], [489, 26789, 23332], 3] will be parsed as
 * [[1],[],[489, 26789, 23332]]
 * Meaning also that :
 * [40, \"[1, 2, 3]\", {somevalue: [123]}, [1],[[0],[0]], [489, 26789, 23332]]
 * will be parsed as [[1],[],[489, 26789, 23332]].
 *
 * Which values are extracted in the last dimensions is up to the 
 * JSONArraysExtractor passed.
 * 
 * min_dimensions(String) and min_dimensions(org.json.JSONArray) will return an
 * integer telling how many nested dimensions will be parsed, if you provide 
 * the same argument to to_array(...).
 *
 * Example :
 * <pre>{@code
 * import com.miouyouyou.libraries.json.JSONNestedArraysToArray;
 * import com.miouyouyou.libraries.json.interfaces.JSONArraysExtractor;
 * import com.miouyouyou.libraries.json.extractors.JSONCharAndNumberExtractor;
 * 
 * public class Test {
 *   public static void main(String[] args) {
 *     String json_array = 
 *       "[[\"H\", \"e\", \"l\", \"l\", \"o\"],[9749],[[\"N\", \"/\", \"A\"]]]";
 *     JSONArraysExtractor codepoint_extractor = 
 *       new JSONCharAndNumberExtractor(char.class);
 *     char[][] hello_beverage = 
 *       (char[][]) JSONNestedArraysToArray.to_array(json_array,
 *                                                   codepoint_extractor);
 *     // Should display : 
 *     // Hello
 *     // ☕
 *     display(hello_beverage);
 *   }
 *   public static void display(char[][] arrays) {
 *     for (char[] array : arrays) {
 *       System.out.println(array);
 *     }
 *   }
 * }
 * }</pre>
 */
public class JSONNestedArraysToArray {

  /** Returns the estimated common number of dimensions in the provided JSON
   *  Array. The minimum is one.
   *
   * @param json_array  the JSON Array checked.
   * @return the common number of dimensions found in the JSON Array. One at
   *         least.
   * @throws IllegalArgumentException if the JSON Array appears malformed.
   */
  public static int min_dimensions(JSONArray json_array) {
    return min_dimensions(json_array.toString());
  }

  /** Returns the estimated common number of dimensions in the provided JSON
   *  Array. The minimum is one.
   *
   * @param json_array  the JSON Array checked.
   * @return the common number of dimensions found in the JSON Array. One at
   *         least.
   * @throws IllegalArgumentException if the JSON Array appears malformed.
   */
  public static int min_dimensions(String json_array) {
    char open_char = '[', close_char = ']', current_character, 
      awaited_special_char = 0;
    int minimum_dimensions = -1, pos;
    /* True when entering arrays, false when closing arrays */
    boolean opening_arrays = false;
    boolean waiting_for_special_character = false;
    boolean escaping_characters = false;
    
    for (pos = 0; pos < json_array.length(); pos++) {
      if (json_array.charAt(pos) == open_char) {
        opening_arrays = true;
        pos++;
        break;
      }
    }

    String malformed_json_string_error = 
      String.format("Malformed JSONArray string : %s", json_array);
    if (opening_arrays == false) {
      throw new IllegalArgumentException(malformed_json_string_error);
    }

    /* It's a very dumb way to check for the minimum number of consecutive
       dimensions in an array... */
    for (int current_dimension = 1; current_dimension >= 1; pos++) {

      /* If we hit the last character before breaking out of the loop,
         the string is malformed... */
      try {
        current_character = json_array.charAt(pos);
      } catch (IndexOutOfBoundsException e) {
        throw new IllegalArgumentException(malformed_json_string_error);
      }

      if (escaping_characters) {
        escaping_characters = false;
        continue;
      }
      if (current_character == '\\') { escaping_characters = true; continue; }

      if (waiting_for_special_character) {
        if (current_character == awaited_special_char) {
          waiting_for_special_character = false;
        }
        continue;
      }

      if (current_character == '"' ||
          current_character == '{') {
        if (current_character == '"') { awaited_special_char = '"'; }
        if (current_character == '{') { awaited_special_char = '}';  }
        waiting_for_special_character = true;
        continue;
      }


      if (current_character == open_char) {
        opening_arrays = true; 
        current_dimension++;
      } 
      else if (current_character == close_char) {
        /* If we we're just beginning to close the last array we opened... */
        if (opening_arrays == true) {
          /* If no minimum dimensions have been set, 
             we set it at the current dimension */
          if (minimum_dimensions == -1) { 
            minimum_dimensions = current_dimension; 
          }

          /* If a minimum has been set, 
             we check if we've hit an even lower number of dimensions */
          minimum_dimensions = 
            (minimum_dimensions > current_dimension) ? 
            current_dimension :
            minimum_dimensions;
        }

        opening_arrays = false; 
        current_dimension--;

      } else { continue; }
        
    }

    return minimum_dimensions;
  }

  /** Converts the provided JSON Array to a multidimensional Array. 
   *
   * The numbers of dimensions in the returned Array depends on :
   *  - the common number of dimensions in the provided JSON Array.
   * What kind of values will be stored in the last dimensions of the returned
   * Array depends on the Class passed. 
   *
   * The JSONArraysExtractor used by this method is the one proxied by
   * JSONArrayToArray(component_class). See JSONArrayToArray(Class).
   * 
   * @param json_array_string  the JSON Array to convert.
   * @param component_class  a Class used to generate a JSONArraysExtractor
   *                         via JSONArrayToArray(Class), which will determine
   *                         what kind of values will be stored in the last 
   *                         dimensions of the returned Array.
   * @return a multidimensional Array. The number of dimensions, the component
   *         type and the values stored in the last dimensions depend on the
   *         provided arguments.
   * @throws IllegalArgumentException if the provided Class object is null.
   */
  public static Object to_array(String json_array_string,
                                Class component_class) {
    return to_array(LocalParser.create_JSON_Array(json_array_string), 
                    component_class);
  }  

  /** Converts the provided JSON Array to a multidimensional Array. 
   *
   * The numbers of dimensions in the returned Array depends on :
   *  - the common number of dimensions in the provided JSON Array.
   * What kind of values will be stored in the last dimensions of the returned
   * Array depends on the Class passed. 
   *
   * The JSONArraysExtractor used by this method is the one proxied by
   * JSONArrayToArray(component_class). See JSONArrayToArray(Class).
   * 
   * @param json_array  the JSON Array to convert.
   * @param component_class  a Class used to generate a JSONArraysExtractor
   *                         via JSONArrayToArray(Class), which will determine
   *                         what kind of values will be stored in the last 
   *                         dimensions of the returned Array.
   * @return a multidimensional Array. The number of dimensions, the component
   *         type and the values stored in the last dimensions depend on the
   *         provided arguments.
   * @throws IllegalArgumentException if the provided org.json.JSONArray is 
   *                                  null.
   * @throws IllegalArgumentException if the provided Class object is null.
   */
  public static Object to_array(JSONArray json_array, 
                                Class component_class) {
    if (json_array == null) {
      throw new IllegalArgumentException("Expected a JSONArray, got null");
    }
    if (component_class == null) {
      throw new IllegalArgumentException("Expected a class, got null");
    }
    return to_array(json_array,
                    new JSONArrayToArray(component_class));

  }

  /** Converts the provided JSON Array to a multidimensional Array. 
   *
   * The numbers of dimensions in the returned Array, and the values stored
   * in the last dimensions in it depend on :
   *  - the common number of dimensions in the provided JSON Array;
   *  - the provided extractor; 
   * 
   * @param json_array_string  the JSON Array to convert.
   * @param extractor  used to extract the values from the last common number 
   *                   of dimensions of the JSON Array.
   * @return a multidimensional Array. The number of dimensions, the component
   *         type and the values stored in the last dimensions depend on the
   *         provided arguments.
   */  
  public static Object to_array(String json_array_string,
                                JSONArraysExtractor extractor) {
    return to_array(LocalParser.create_JSON_Array(json_array_string), 
                    extractor);
  } 

  /** Converts the provided JSON Array to a multidimensional Array. 
   *
   * The numbers of dimensions in the returned Array, and the values stored
   * in the last dimensions in it depend on :
   *  - the common number of dimensions in the provided JSON Array;
   *  - the provided extractor; 
   * 
   * @param json_array  the JSON Array to convert.
   * @param extractor  used to extract the values from the last common number 
   *                   of dimensions of the JSON Array.
   * @return a multidimensional Array. The number of dimensions, the component
   *         type and the values stored in the last dimensions depend on the
   *         provided arguments.
   */  
  public static Object to_array(JSONArray json_array,
                                JSONArraysExtractor extractor) {
    int dimensions_to_parse       = min_dimensions(json_array);
    if (dimensions_to_parse == 1) {
      return extractor.extract_values(json_array);
    } else {
      return to_array(json_array, 1, dimensions_to_parse, 
                      extractor);
    }
    
  }

  private static Object to_array(JSONArray json_array,
                                 int current_dimension,
                                 int max_dimensions,
                                 JSONArraysExtractor last_dimension_extractor) {
    /* Temporary list used to store all the valid sub-arrays created */
    List<Object> temporary_list = new ArrayList<Object>();
    JSONArray array_to_parse = null;

    for (int index = 0; index < json_array.length(); index++) {

      array_to_parse = json_array.optJSONArray(index); 

      if (array_to_parse != null) {
        if (current_dimension == max_dimensions - 1) {
          temporary_list
            .add(last_dimension_extractor.extract_values(array_to_parse));
        }
        else {
          temporary_list
            .add(to_array(array_to_parse, 
                          current_dimension+1, 
                          max_dimensions,
                          last_dimension_extractor));
        }
      }
    }

    if (temporary_list.isEmpty()) { 
      return multidimensional_array(last_dimension_extractor.component_type(),
                                    max_dimensions - current_dimension);
    } else { return create_array_from(temporary_list); }

  }

  private static Object create_array_from(List<Object> list) {
    int list_size = list.size();
    Object array = Array.newInstance(list.get(0).getClass(),
                                     list_size);
    for (int index = 0; index < list_size; index++) {
      Array.set(array, index, list.get(index));
    }
    return array;
  }

  /** Creates an empty multidimensional array from a specific class.
   * The last dimension length will be 0, all others will be 1.
   * If you want to use the returned Array direclty, you'll have to cast it.
   * E.g. : ((int[][][]) multidimensional_array(int.class, 3))
   * @param array_component_class  The returned array component type.
   * @param dimensions  The number of dimensions to create. 
   * @return a multidimensional array as an Object.
   */
  private static Object multidimensional_array(Class array_component_class,
                                               int dimensions) {
    Object last_array, newest_array;
    newest_array = Array.newInstance(array_component_class, 0);
    for (int dimension = 1; dimension < dimensions; dimension++) {
      last_array   = newest_array;
      newest_array = Array.newInstance(last_array.getClass(), 1);
      Array.set(newest_array, 0, last_array);
    }

    return newest_array;
  }

}
