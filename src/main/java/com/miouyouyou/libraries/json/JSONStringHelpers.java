package com.miouyouyou.libraries.json;

/** This class contains helper methods dealing with character strings in JSON 
 *  structures.
 */
public class JSONStringHelpers {

  private static String[] escaped_control_characters;
  private static String escaping_format;
 
  static {
    escaped_control_characters = new String[]
      {"\\u0000", "\\u0001", "\\u0002", "\\u0003", "\\u0004", "\\u0005", 
       "\\u0006", "\\u0007", "\\b", "\\t", "\\n", "\\u000b", "\\f", 
       "\\r", "\\u000e", "\\u000f", "\\u0010", "\\u0011",  "\\u0012", 
       "\\u0013", "\\u0014", "\\u0015", "\\u0016", "\\u0017", "\\u0018", 
       "\\u0019", "\\u001a", "\\u001b", "\\u001c", "\\u001d", "\\u001e", 
       "\\u001f"};
    escaping_format = "\\%c";
  }

  /** Provides an escaped version of a String, usable in a JSON structure.
   *
   * @param unescaped_string  The string to escape.
   * @return an escaped version of the String that can be used in a JSON
   *         structure.
   * @throws NullPointException if the provided String is null.
   */
  public static String escape_for_JSON(String unescaped_string) {
    StringBuilder escaped_json_string = new StringBuilder();

    char current_char = 0;
    for (int index = 0; index < unescaped_string.length(); index++) {
      current_char = unescaped_string.charAt(index);
      
      // Characters not allowed if not escaped
      if (current_char < 0x20) {
        escaped_json_string.append(escaped_control_characters[current_char]);
        continue;
      }
      else if (current_char == '\\' || current_char == '"') {
        escaped_json_string.append(String.format(escaping_format,
                                                 current_char));
        continue;
      }

      // Characters allowed
      escaped_json_string.append(current_char);
    }
    return escaped_json_string.toString();
  }
}
